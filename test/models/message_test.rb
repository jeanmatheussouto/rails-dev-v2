require 'test_helper'

class MessageTest < ActiveSupport::TestCase
  #
  # Validations
  #

  test "title presence" do
    message = Message.new
    assert message.invalid?
    assert message.errors[:title].present?
  end

  test "content presence" do
    message = Message.new
    assert message.invalid?
    assert message.errors[:content].present?
  end

  #
  # State machine
  #
  test "initial state" do
    message = Message.create(title: '1', content: '2')
    assert message.unread?
  end

  test "reading a message" do
    message = messages(:one)

    assert message.unread?
    message.read

    assert message.read?
    assert message.read_at.present?
    assert message.read_at.is_a? Time
  end

  test "archive a message" do
    message = messages(:one)

    assert_not message.archived?
    message.archive

    assert message.archived?
    assert message.archived_at.present?
    assert message.archived_at.is_a? Time
  end
end
