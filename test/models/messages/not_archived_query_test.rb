require 'test_helper'

class Messages::NotArchivedQueryTest < ActiveSupport::TestCase
  setup do
    @query = Messages::NotArchivedQuery.new
  end

  test "#find_each" do
    msg_one = messages(:one)
    
    assert_respond_to @query, :find_each
  end

  test "#by_created_at" do
    msg_one = messages(:one)
    msg_two = messages(:two)

    assert_respond_to @query, :by_created_at
    assert_equal @query.by_created_at, [msg_one, msg_two]
    assert_not_equal @query.by_created_at, [msg_two, msg_one]
  end
end
