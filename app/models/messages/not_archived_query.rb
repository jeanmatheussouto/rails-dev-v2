module Messages
  class NotArchivedQuery

    def initialize(relation = Message.all)
      @relation = relation
    end

    def find_each(&block)
      messages.find_each(&block)
    end

    def by_created_at(order = :desc)
      messages.order(created_at: order)
    end

    private

    def messages
      @relation.where.not(state: :archived)
    end

  end
end
