App.messages.index = App.messages.index || {}

class App.messages.index.archiveAllMessages

  constructor: (@options = {}) ->
    @make()
    @bind()

  make: () ->
    @elementClass = '.js-archive-all-messages'
    @$element = $(@elementClass)

  bind: () ->
    @$element.on 'click', (event) =>
      @table = $(".js-table-messages")
      event.preventDefault()
      $.ajax({
        url: @$element.attr('href'),
        type: 'put',
        dataType: "json"
      }).always( (data) =>
        @table.remove()
      )
